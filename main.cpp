#include <iostream>

using std::cout;
using std::endl;

template<typename T>
T max(T x, T y) {
    return (x > y) ? x : y;
}

// template with one predefined type
template<typename T>
T divide(T x, int y) {
    return x / y;
}

// template with two dedicated types
template<typename T, typename U>
U divide(T x, U y) {
    return x / y;
}


int main() {
    // Using max function template
    cout << "Result of 'max<int>(1, 5)': " << max<int>(1, 5) << endl;
    cout << "Result of 'max<double>(1.5, 1.4)': " << max<double>(1.5, 1.4) << endl << endl;

    // Using max function template without type (deduction)
    cout << "Result of 'max<>(1, 5)': " << max<>(1, 5) << endl;
    cout << "Result of 'max<>(1.5, 1.4)': " << max<>(1.5, 1.4) << endl << endl;

    // Using function template with predefined type
    cout << "Result of 'divide<double>(1, 2)': " << divide<double>(1, 2) << endl << endl;

    // Using function template with two types
    cout << "Result of 'divide<int,double>(1, 2)': " << divide<int,double>(1, 2) << endl << endl;

    return 0;
}
